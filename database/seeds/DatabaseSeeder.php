<?php

use App\Model\Page;
use App\Model\Post;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//		factory(\App\Model\Post_cat::class, 10)->create();
		factory(Post::class, 1000)->create();
//		factory(Page::class, 1000)->create();
        // $this->call(UsersTableSeeder::class);
    }
}
