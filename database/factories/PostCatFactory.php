<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Post_cat::class, function (Faker $faker) {
	return [
		'title'=> rtrim($faker->sentence(rand(5, 10),".")),
		'slug'=> str_slug(rtrim($faker->sentence(rand(5, 10),"."))),
		'parent_id' => 0,
		'user_id' => App\Model\Admin::all()->random()->id,
	];
});
