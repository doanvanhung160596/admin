<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Page::class, function (Faker $faker) {
	return [
		'title'=> rtrim($faker->sentence(rand(5, 10),".")),
		'slug'=> str_slug(rtrim($faker->sentence(rand(5, 10),"."))),
		'content' => $faker->paragraph,
		'status' => '1',
		'viewer'=> rand(0, 100),
		'user_id' => App\Model\Admin::all()->random()->id,
	];
});
