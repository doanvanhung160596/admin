<?php

use Faker\Generator as Faker;

$factory->define(App\Model\Post::class, function (Faker $faker) {
	return [
		'title'=> rtrim($faker->sentence(rand(5, 10),".")),
		'slug'=> str_slug(rtrim($faker->sentence(rand(5, 10),"."))),
		'title_seal'=> str_slug(rtrim($faker->sentence(rand(5, 10)," "))),
		'content' => $faker->paragraph,
		'description' => rtrim($faker->sentence(rand(5, 10),".")),
		'status' => '1',
		'viewer'=> rand(0, 100),
		'image'=> 'http://hanoimoi.com.vn/Uploads/lequyen/2019/2/14/9.jpg',
		'user_id' => App\Model\Admin::all()->random()->id,
		'post_cat_id' => App\Model\Post_cat::where('parent_id','>',0)->get()->random()->id,
		'category_id' => App\Model\Post_cat::where('parent_id',0)->get()->random()->id,
	];
});
