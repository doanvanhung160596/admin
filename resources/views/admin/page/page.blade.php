@extends('admin.layouts.index')
@section('title','Quản lý trang')
@section('content')
    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">@lang('admin.page')</h1>
                    <div class="text-right" style="margin-right: 30px;">
                        <button class="btn btn-primary" id="add_page">@lang('admin.add_page')</button>
                        <input type="hidden" id="add_page_hidden" value="{{route('admin.page.create')}}">
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.home') }}"><i class="demo-pli-home"></i></a></li>
                    <li>Trang</li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->

            </div>


            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">

            {{--<div class="panel">--}}
            {{--<div class="panel-heading">--}}
            {{--<h3 class="panel-title">Full width content</h3>--}}
            {{--</div>--}}
            {{--<!--Summernote-->--}}
            {{--<!--===================================================-->--}}
            {{--<div id="demo-summernote">--}}
            {{--<h4><span style="color: rgb(206, 198, 206); font-family: inherit; line-height: 1.1;">Please, write text here!</span><br></h4><h4><font color="#9c9c94"></font></h4>--}}
            {{--</div>--}}
            {{--<!--===================================================-->--}}
            {{--<!-- End Summernote -->--}}
            {{--</div>--}}


            <!-- Basic Data Tables -->
                <!--===================================================-->
                <div class="panel data-table">
                    <div class="panel-heading">
                        <h3 class="panel-title data-table-title">Danh sách trang</h3>
                    </div>
                    <div class="panel-body">
                        <select class="selectpicker" name="actions" id="actions">
                            <option value="1">Đăng</option>
                            <option value="-1">Chờ xét duyệt</option>
                            <option value="4">Xóa</option>
                        </select>
                        <button class="btn btn-primary" style="margin-bottom: 15px;" id="action">Thao Tác</button>
                        {!! $dataTable->table() !!}
                    </div>
                </div>
                <!--===================================================-->
                <!-- End Striped Table -->
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        <div class="modal" id="myModal">
            <div class="modal-dialog modal-lg" style="width: 80%;">
                <div class="modal-content">
                    <div class="loader loading-area">
                        <img class="loading-image" src="{{ asset('admin/img/loading/loading.gif') }}">
                        <p class="loading-text">@lang('admin.loading') ...</p>
                    </div>
                    <div class="modal-header" style="border-bottom: 1px solid #eee;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">@lang('admin.add_page')</h4>
                    </div>
                    <div class="container"></div>
                    <div class="modal-body" id="modal-body-id">

                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<a href="#" data-dismiss="modal" class="btn btn-info">@lang('admin.close')</a>--}}
                    {{--<button class="btn btn-primary" id="save">@lang('admin.save')</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        {{--Popup Delete--}}
        <div class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>

                    <div class="modal-body">
                        <p>One fine body ...........</p>
                    </div>

                    <div class="modal-footer">
                        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                        <button class="btn btn-primary" type="button">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        {{--@include('admin.layouts.aside')--}}
        {{--@include('admin.layouts.navigation')--}}
    </div>
@endsection

@section('js')
    {!! $dataTable->scripts() !!}
    <script>
        function updateStatus(id){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{route('admin.page.updateStatus')}}",
                dataType: 'text',
                data: {
                    id : id
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(data) {
                    window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                    setTimeout(function(){
                        iziToast.success({
                            position: 'topRight',
                            title: '@lang('admin.info')',
                            message: data
                        });
                    }, 1000);
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // ADD PAGE MODAL
        $('#add_page').click(function () {
            var action = $('#add_page_hidden').val();
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.add_page')');

            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        // VIEW PAGE MODAL
        function viewPage(obj){
            var action = obj.getAttribute('data-url');
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.view_page')');
            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {

                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // UPDATE PAGE MODAL
        function updatePage(obj){
            var action = obj.getAttribute('data-url');
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.edit_page')');
            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {

                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // Delete Page
        function deletePage(id){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{route('admin.page.deletePage')}}",
                dataType: 'json',
                data: {
                    id : id
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(response) {
                    if(response.result == 'success'){
                        window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                        setTimeout(function(){
                            iziToast.success({
                                position: 'topRight',
                                title: '@lang('admin.info')',
                                message: response.info
                            });
                        }, 1000);
                    }else{
                        window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                        setTimeout(function(){
                            iziToast.error({
                                position: 'topRight',
                                title: '@lang('admin.info')',
                                message: response.info
                            });
                        }, 1000);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // LIST CHECKBOX ACTION
        $('#checkAll').click(function () {
            var checkBoxes = $(".checkItem");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
        // ACTION STATUS AND DELETE
        $('#action').click(function () {
            var action = $('#actions').val();
            var item_checked = [];
            $.each($("input[name='checkItem']:checked"), function(){
                item_checked.push($(this).val());
            });
            console.log(item_checked);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{route('admin.page.action')}}",
                dataType: 'json',
                data: {
                    action       : action,
                    item_checked : item_checked
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(response) {
                    if(response.result == 'success'){
                        window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                        setTimeout(function(){
                            iziToast.success({
                                position: 'topRight',
                                title: '@lang('admin.info')',
                                message: response.info
                            });
                        }, 1000);
                    }else{
                        // setTimeout(function(){
                        iziToast.error({
                            position: 'topRight',
                            title: '@lang('admin.info')',
                            message: response.info
                        });
                        // }, 1000);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    </script>
@stop
