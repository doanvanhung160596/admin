        <div id="page-content">
            <div class="panel blog blog-details">
                <div class="panel-body">
                    <div class="blog-title media-block">
                        <div class="media-right textright">
                            <a href="#" class="btn btn-icon demo-psi-twitter icon-lg add-tooltip" data-original-title="Twitter" data-container="body"></a>
                            <a href="#" class="btn btn-icon demo-psi-instagram icon-lg add-tooltip" data-original-title="Instagram" data-container="body"></a>
                        </div>
                        <div class="media-body">
                            <a href="#" class="btn-link">
                                <h1>{{ $page->title }}</h1>
                            </a>
                            <p>@lang('admin.by')
                                <a href="#" class="btn">{{ $page->user->name }}
                                    <span class="label label-success">{{ $page->CreatedDate }}</span>
                                </a>
                            </p>
                            <p>
                                @lang('admin.number_view'): <span class="mar-rgt"> {{ $page->viewer }} @lang('admin.view')</span>
                                <br>
                            </p>
                            {{--<p>--}}
                                {{--@lang('admin.status'): <span class="mar-rgt"> {{ $page->Statu }} @lang('admin.view')</span>--}}
                            {{--</p>--}}
                        </div>
                    </div>
                    <hr>
                    <div class="blog-content">
                        <div class="blog-body">
                            {!! $page->content !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>


