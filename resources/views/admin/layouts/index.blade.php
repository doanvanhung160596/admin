<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('admin/img/logo.png') }}" />

    <title>@yield('title','Admin CMS')</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('admin/css/nifty.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/css/nifty-theme.css') }}" rel="stylesheet">

    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('admin/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">


    <!--=================================================-->



    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('admin/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('admin/plugins/pace/pace.min.js')}}"></script>


    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('admin/css/demo/nifty-demo.min.css')}}" rel="stylesheet">



    <!--Custom scheme [ OPTIONAL ]-->
    <link href="{{ asset('admin/css/themes/type-c/theme-navy.min.css')}}" rel="stylesheet">

    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('admin/plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css')}}" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('admin/css/jquery.dataTables.min.css')}}">
    {{--iziToast - THÔNG BÁO--}}
    <link rel="stylesheet" href="{{ asset('iziToast/dist/css/iziToast.css')}}">
    {{--LOADING MODAL--}}
    <link rel="stylesheet" href="{{ asset('admin/css/loading.css')}}">
    <!--Bootstrap Validator [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/bootstrap-validator/bootstrapValidator.min.css')}}" rel="stylesheet">
    {{--Datatables --}}
    <link rel="stylesheet" href="{{ asset('admin/css/datatable.css')}}">
    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <!--Bootstrap Datepicker [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
    <!--Bootstrap Select [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
    <!--Select2 [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet">
    <!--Summernote [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/summernote/summernote.min.css')}}" rel="stylesheet">
    <!--Dropzone [ OPTIONAL ]-->
    <link href="{{ asset('admin/plugins/dropzone/dropzone.min.css')}}" rel="stylesheet">
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
<div id="container" class="effect aside-float aside-bright mainnav-lg">
    @include('admin.layouts.header')
    @yield('content')
    @include('admin.layouts.siderbar')
    @include('admin.layouts.footer')

    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->





<!--JAVASCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('admin/js/jquery.min.js')}}"></script>


<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('admin/js/bootstrap.min.js')}}"></script>


<!--NiftyJS [ RECOMMENDED ]-->
<script src="{{ asset('admin/js/nifty.min.js')}}"></script>




<!--=================================================-->

<!--Demo script [ DEMONSTRATION ]-->
<script src="{{ asset('admin/js/demo/nifty-demo.min.js')}}"></script>


<!--Flot Chart [ OPTIONAL ]-->
<script src="{{ asset('admin/plugins/flot-charts/jquery.flot.min.js')}}"></script>
<script src="{{ asset('admin/plugins/flot-charts/jquery.flot.categories.min.js')}}"></script>
<script src="{{ asset('admin/plugins/flot-charts/jquery.flot.orderBars.min.js')}}"></script>
<script src="{{ asset('admin/plugins/flot-charts/jquery.flot.tooltip.min.js')}}"></script>
<script src="{{ asset('admin/plugins/flot-charts/jquery.flot.resize.min.js')}}"></script>


<!--Specify page [ SAMPLE ]-->
{{--<script src="{{ asset('admin/js/demo/dashboard-2.js')}}"></script>--}}
<!--DataTables [ OPTIONAL ]-->
<script src="{{ asset('admin/plugins/select2/js/select2.min.js')}}"></script>
<!--Bootstrap Timepicker [ OPTIONAL ]-->
<script src="{{ asset('admin/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js')}}"></script>
<!--Bootstrap Datepicker [ OPTIONAL ]-->
<script src="{{ asset('admin/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{ asset('admin/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('admin/js/demo/tables-datatables.js')}}"></script>
<script src="{{ asset('admin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('admin/plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<!--Form File Upload [ SAMPLE ]-->
<!--Summernote [ OPTIONAL ]-->
<script src="{{ asset('admin/plugins/summernote/summernote.min.js') }}"></script>
<script src="{{ asset('admin/js/demo/form-text-editor.js')}}"></script>
{{--iziToast--}}
<script src="{{ asset('iziToast/dist/js/iziToast.js')}}"></script>
<script>
    // Loading in AJAX
    var $loading = $('.loader').hide();
    var $modal_header = $('.modal-header').hide();
    $(document)
        .ajaxStart(function () {
            $loading.show();
            $modal_header.hide();
            $('.modal-body').css('display', 'none');
            $('.modal-content').css('width', '200px');
            $('.modal-content').css('left', 'calc(50% - 100px)');
            $('.modal-footer').css('display', 'none');
        })
        .ajaxStop(function () {
            $loading.hide();
            $modal_header.show();
            $('.modal-body').css('display', 'block');
            $('.modal-content').css('width', 'auto');
            $('.modal-content').css('left', '0px');
            $('.modal-footer').css('display', 'block');
        });
</script>
@yield('js')
</body>

</html>