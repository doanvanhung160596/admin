@extends('layouts.admin')
@section('title','Đăng Nhập')

@section('content')
    <div id="container" class="cls-container">

        <!-- BACKGROUND IMAGE -->
        <!--===================================================-->
        <div id="bg-overlay" class="bg-img" style="background-image: url(login-page/img/bg-img/bg-img-5.jpg)"></div>


        <!-- LOGIN FORM -->
        <!--===================================================-->
        <div class="cls-content">
            <div class="cls-content-sm panel">
                <div class="panel-body">
                    <div class="mar-ver pad-btm">
                        <h1 class="h3">{{ __('Đăng Nhập') }}</h1>
                        <p>{{ __('Vui lòng đăng nhập vào tài khoản của bạn !') }}</p>
                    </div>
                    <form action="{{ route('admin.login') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="email" class="col-md-6 col-form-label text-left">{{ __('Địa chỉ Email') }}</label>
                            <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-6 col-form-label text-left">{{ __('Mật khẩu') }}</label>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="checkbox pad-btm text-left">
                            <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox" name="remember"  {{ old('remember') ? 'checked' : '' }}>
                            <label for="demo-form-checkbox">{{ __('Nhớ mật khẩu') }}</label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            {{ __('Đăng nhập') }}
                        </button>
                    </form>
                </div>

                <div class="pad-all">
                    {{--@if (Route::has('admin.password.request'))--}}
                        {{--<a class="btn-link mar-rgt" href="{{ route('admin.password.request') }}">--}}
                            {{--{{ __('Bạn quên mật khẩu ?') }}--}}
                        {{--</a>--}}
                    {{--@endif--}}
                        {{--@if (Route::has('admin.register'))--}}
                        {{--<a class="btn-link mar-lft" href="{{ route('admin.register') }}">--}}
                            {{--{{ __('Bạn chưa có tài khoản ?') }}--}}
                        {{--</a>--}}
                        {{--@endif--}}

                    <div class="media pad-top bord-top">
                        {{--<div class="pull-right">--}}
                            {{--<a href="#" class="pad-rgt"><i class="psi-facebook icon-lg text-primary"></i></a>--}}
                            {{--<a href="#" class="pad-rgt"><i class="psi-twitter icon-lg text-info"></i></a>--}}
                            {{--<a href="#" class="pad-rgt"><i class="psi-google-plus icon-lg text-danger"></i></a>--}}
                        {{--</div>--}}
                        {{--<div class="media-body text-left text-bold text-main">--}}
                            {{--Để sau--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <!--===================================================-->
    </div>
    <!--===================================================-->
    <!-- END OF CONTAINER -->
@endsection


