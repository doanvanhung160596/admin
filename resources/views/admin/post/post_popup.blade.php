<form class="panel-body form-horizontal form-padding"  id="form-action" action="{{ $action }}" method="post">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@if($id)
    {{ method_field('PATCH') }}
@endif
<!--Text Input-->
    <div class="form-group">
        <label class="col-md-1 control-label" for="demo-text-input">Tiêu đề</label>
        <div class="col-md-11">
            <input type="text" name="title" value="@if($id){{ $post->title }}@endif" id="demo-text-input" class="form-control" placeholder="Text">
            <small class="help-block">This is a help text</small>
        </div>
    </div>

    <div class="form-group">
        <label class="col-md-1 control-label" for="demo-text-input">Mô tả</label>
        <div class="col-md-11">
            <input type="text" name="description" value="@if($id){{ $post->description }}@endif" id="demo-text-input" class="form-control" placeholder="Text">
            <small class="help-block">This is a help text</small>
        </div>
    </div>
    <div class="form-group" style="margin-bottom: 0px;">
        <label class="col-md-1 control-label" for="demo-text-input">Ảnh mô tả</label>
         @if(!empty($id))
            <div id="uploadFile" style="display: block;">
                <label for="image"><img id="blah" style="cursor: pointer;" src="{{asset($post->image)}}" alt="Vui lòng chọn ảnh" width="300" height="200" /></label>
            </div>
         @else
            <div id="uploadFile" style="display: block;">
                <label for="image"><img id="blah" style="cursor: pointer;" src="{{asset('admin/img/image-ex.png')}}" alt="Vui lòng chọn ảnh" width="300" height="200" /></label>
            </div>
         @endif
    </div>
    <div class="form-group">
        <label class="col-md-1 control-label" for="demo-text-input"></label>
        <span class="pull-left btn btn-primary btn-file">
				Chọn file ảnh...<input type="file" id="image" name="fileUpload" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">
        </span>
    </div>
    <!--Textarea-->
    <div class="form-group">
        <label class="col-md-1 control-label" for="demo-textarea-input">Nội dung</label>
        <div class="col-md-11">
            <textarea id="demo-summernote" name="content" rows="9" class="form-control" placeholder="Your content here..">
                @if($id){{ $post->content }}@endif
            </textarea>
        </div>
    </div>

    <div class="form-group pad-ver">
        <label class="col-md-1 control-label">Chọn cấp cha</label>
        <div class="col-md-11 col-sm-6">
            <select class="form-control" name="post_cat_id" id="post_cat_id">
                <option value="">Chọn cấp</option>
                @foreach($categories as $item)
                    <option value="{{ $item->id }}" @if($id)  @if($post->post_cat_id == $item->id) selected @endif @endif >{{ $item->title }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group pad-ver">
        <label class="col-md-1 control-label">Trang thái</label>
        <div class="col-md-11">
            <!-- Radio Buttons -->
            <div class="radio">
                <input id="demo-form-radio" class="magic-radio" type="radio" value="1"  @if($id)  @if($post->status == '1') checked @endif @endif name="status" checked="">
                <label for="demo-form-radio">Đăng</label>
            </div>
            <div class="radio">
                <input id="demo-form-radio-2" class="magic-radio" type="radio" value="-1" @if($id)  @if($post->status == '-1') checked @endif @endif name="status">
                <label for="demo-form-radio-2">Chờ</label>
            </div>
        </div>
    </div>
    <div class="form-group" style="padding-top: 40px; text-align: right; border-top: 1px solid #e8e8e8;">
        <a href="#" data-dismiss="modal" class="btn btn-info">@lang('admin.close')</a>
        <button class="btn btn-primary" id="save">@lang('admin.save')</button>
    </div>
</form>
<script>
    $('#demo-summernote').summernote({
        height : '230px'
    });
</script>
<style>
    .modal-dialog { z-index: 1040; }
</style>
<script type="text/javascript">
    $(document).ready(function() {
        $( "#save").on("click",function (e) {
            e.preventDefault();
            var $flag = true;
            if ($flag == true) {
                var action = $("#form-action").attr("action");
                var request_method = $("#form-action").attr("method");
                var form_data = new FormData(document.getElementById('form-action'));
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: action,
                    type: request_method,
                    data: form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                }).done(function (data) {
                    if(data.success){
                        if(data.result == 'add'){
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: data.success
                            });
                            var table = $("#dataTableBuilder").DataTable();
                            table.ajax.reload();
                        }else{
                            $('#myModal').modal('hide');
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: data.success
                            });
                            var table = $("#dataTableBuilder").DataTable();
                            table.ajax.reload();
                        }

                    }else{
                        jQuery.each(data.errors, function(key, value){
                            iziToast.error({
                                position: 'topRight',
                                title: '@lang('admin.info')',
                                message: value
                            });
                        });
                    }
                });
            }
        });
    });
</script>