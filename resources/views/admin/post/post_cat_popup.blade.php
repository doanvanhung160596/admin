<form class="panel-body form-horizontal form-padding"  id="form-action" action="{{ $action }}" method="post">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input type="hidden" id="id-post-cat" value="@if($id){{ $id }}@endif">
@if($id)
    {{ method_field('PATCH') }}
@endif
<!--Text Input-->
    <div class="form-group">
        <label class="col-md-1 control-label" for="demo-text-input">Tiêu đề</label>
        <div class="col-md-11">
            <input type="text" name="title" value="@if($id){{$post_cat->title}}@endif" id="demo-text-input" class="form-control" placeholder="Text">
            <small class="help-block">This is a help text</small>
        </div>
    </div>

    <div class="form-group pad-ver">
        <label class="col-md-1 control-label">Chọn cấp cha</label>
        <div class="col-md-11 col-sm-6">
            <select class="form-control" name="parent_id" id="parent-id">
            </select>
        </div>
    </div>
    <div class="form-group" style="padding-top: 40px; text-align: right; border-top: 1px solid #e8e8e8;">
        <a href="#" data-dismiss="modal" class="btn btn-info">@lang('admin.close')</a>
        <button class="btn btn-primary" id="save">@lang('admin.save')</button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        load_list_parent_cat();
    });
    // LIST Parent CAT
    function load_list_parent_cat(){
        $('#parent-id').html('');
        var id = $('#id-post-cat').val();
        $.ajax({
            url: "{{ route('admin.postCat.getListParent') }}",
            dataType: 'html',
            method: 'GET',
            data: {
                id : id
            },
            beforeSend: function () {
            },
            complete: function () {
            },
            success: function (data) {
                $('#parent-id').prepend(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
    $(document).ready(function() {
        $( "#save").on("click",function (e) {
            e.preventDefault();
            var $flag = true;
            if ($flag == true) {
                var action = $("#form-action").attr("action");
                var request_method = $("#form-action").attr("method");
                var form_data = new FormData(document.getElementById('form-action'));
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: action,
                    type: request_method,
                    data: form_data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                }).done(function (data) {
                    if(data.success){
                        load_list_post_cat();
                        load_list_parent_cat();
                        if(data.result == 'add'){
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: data.success
                            });
                        }else{
                            $('#myModal').modal('hide');
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: data.success
                            });
                        }
                    }else{
                        jQuery.each(data.errors, function(key, value){
                            iziToast.error({
                                position: 'topRight',
                                title: '@lang('admin.info')',
                                message: value
                            });
                        });
                    }
                });
            }
        });
    });
</script>