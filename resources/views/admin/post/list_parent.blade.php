<option value="0">Chọn cấp</option>
@foreach($parents as $item)
    <option value="{{ $item->id }}" @if( isset($post_cat)) @if($post_cat->parent_id == $item->id) selected @endif @endif>{{ $item->title }}</option>
@endforeach