@extends('admin.layouts.index')
@section('title','Quản lý danh mục bài viết')
@section('content')
    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">@lang('admin.post_cat')</h1>
                    <div class="text-right" style="margin-right: 30px;">
                        <button class="btn btn-primary" id="add_post_cat">@lang('admin.add_post_cat')</button>
                        <input type="hidden" id="add_post_cat_hidden" value="{{route('admin.post_cat.create')}}">
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.home') }}"><i class="demo-pli-home"></i></a></li>
                    <li>@lang('admin.post_cat')</li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->

            </div>


            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">

            {{--<div class="panel">--}}
            {{--<div class="panel-heading">--}}
            {{--<h3 class="panel-title">Full width content</h3>--}}
            {{--</div>--}}
            {{--<!--Summernote-->--}}
            {{--<!--===================================================-->--}}
            {{--<div id="demo-summernote">--}}
            {{--<h4><span style="color: rgb(206, 198, 206); font-family: inherit; line-height: 1.1;">Please, write text here!</span><br></h4><h4><font color="#9c9c94"></font></h4>--}}
            {{--</div>--}}
            {{--<!--===================================================-->--}}
            {{--<!-- End Summernote -->--}}
            {{--</div>--}}


            <!-- Basic Data Tables -->
                <!--===================================================-->
                <div class="panel data-table">
                    <div class="panel-heading">
                        <h3 class="panel-title data-table-title" style="text-transform: uppercase;">Danh mục bài viết</h3>
                    </div>
                    <div class="panel-body">
                        <select class="selectpicker" name="actions" id="actions">
                            <option value="4">Xóa</option>
                        </select>
                        <button class="btn btn-primary" style="margin-bottom: 15px;" id="action">Thao Tác</button>
                        <table id="demo-dt-basic_info"
                               {{--class="display" width="100%" cellspacing="0"--}}
                               class="table dataTable no-footer" id="dataTableBuilder" role="grid" aria-describedby="dataTableBuilder_info" style="width: 1049px;"
                               {{--class="table table-striped table-bordered dataTable no-footer dtr-inline" cellspacing="0" width="100%" role="grid" aria-describedby="demo-dt-basic_info" style="width: 100%;"--}}
                        >
                            <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" style="cursor: pointer;" class="magic-checkbox" name="checkAll" id="checkAll">
                                    <label for="checkAll"></label>
                                </th>
                                <th>ID</th>
                                <th>Tiêu đề</th>
                                <th>Cấp</th>
                                <th>Người tạo</th>
                                <th>Tạo lúc</th>
                                <th>Cập nhật</th>
                                <th>Tác vụ</th>
                            </tr>
                            </thead>
                            <tbody id="list-post-cat">

                            </tbody>
                        </table>
                    </div>
                </div>
                <!--===================================================-->
                <!-- End Striped Table -->
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        <div class="modal" id="myModal">
            <div class="modal-dialog modal-lg" style="width: 60%;">
                <div class="modal-content">
                    <div class="loader loading-area">
                        <img class="loading-image" src="{{ asset('admin/img/loading/loading.gif') }}">
                        <p class="loading-text">@lang('admin.loading') ...</p>
                    </div>
                    <div class="modal-header" style="border-bottom: 1px solid #eee;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">@lang('admin.add_page')</h4>
                    </div>
                    <div class="container"></div>
                    <div class="modal-body" id="modal-body-id">
                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<a href="#" data-dismiss="modal" class="btn btn-info">@lang('admin.close')</a>--}}
                    {{--<button class="btn btn-primary" id="save">@lang('admin.save')</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        {{--@include('admin.layouts.aside')--}}
        {{--@include('admin.layouts.navigation')--}}
    </div>
@stop

@section('js')
    <script>
        $(document).ready(function () {
            load_list_post_cat();
        });
        // LIST POST CAT
        function load_list_post_cat(){
            $('#list-post-cat').html('');
            $.ajax({
                url: '{{ route('admin.postCat.getListPostCat') }}',
                dataType: 'html',
                method: 'GET',
                data: {
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#list-post-cat').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // ADD POST CAT MODAL
        $('#add_post_cat').click(function () {
            var action = $('#add_post_cat_hidden').val();
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.add_post_cat')');

            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        // UPDATE POST CAT MODAL
        function updatePage(obj){
            var action = obj.getAttribute('data-url');
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.edit_post_cat')');
            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {

                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // Delete Page
        function deletePostCat(id){
            $info = iziToast.question({
                overlay: true,
                toastOnce: true,
                id: 'question',
                title: '@lang('admin.hi')',
                message: '@lang('admin.action_question')',
                position: 'center',
                buttons: [
                    ['<button><b>@lang('admin.yes')</b></button>', function (instance, toast) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            url: "{{route('admin.postCat.deletePostCat')}}",
                            dataType: 'json',
                            data: {
                                id : id
                            },
                            beforeSend: function() {
                            },
                            complete: function() {
                            },
                            success: function(response) {
                                if(response.result == 'success'){
                                    load_list_post_cat();
                                    setTimeout(function(){
                                        iziToast.success({
                                            position: 'topCenter',
                                            title: '@lang('admin.info')',
                                            message: response.info
                                        });
                                    }, 1000);
                                }else{
                                    setTimeout(function(){
                                        iziToast.error({
                                            position: 'topCenter',
                                            title: '@lang('admin.info')',
                                            message: response.info
                                        });
                                    }, 1000);
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast);
                    }, true],
                    ['<button>@lang('admin.no')</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast);

                    }]
                ]
            });
        }
        // LIST CHECKBOX ACTION
        $('#checkAll').click(function () {
            var checkBoxes = $(".checkItem");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
        // ACTION STATUS AND DELETE
        $('#action').click(function () {
            var action = $('#actions').val();
            var item_checked = [];
            $.each($("input[name='checkItem']:checked"), function(){
                item_checked.push($(this).val());
            });
            console.log(item_checked);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{route('admin.postCat.action')}}",
                dataType: 'json',
                data: {
                    action       : action,
                    item_checked : item_checked
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(response) {
                    if(response.result == 'success'){
                        load_list_post_cat();
                        setTimeout(function(){
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: response.info
                            });
                        }, 1000);
                    }else{
                        iziToast.error({
                            position: 'topCenter',
                            title: '@lang('admin.info')',
                            message: response.info
                        });
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    </script>
@stop
