@foreach($data as $item)
    <tr>
        <td>
            <input name="checkItem" value="{{ $item->id }}" style="cursor: pointer;" class="checkItem magic-checkbox" type="checkbox" id="{{ $item->id }}" />
            <label for="{{ $item->id }}"></label>
        </td>
        <td>{{ $item->id }}</td>
        <td>{{ str_repeat('---- ', $item->level).$item->title }}</td>
        <td>{{ $item->level }}</td>
        <td>{{ $item->user->name }}</td>
        <td>{{ $item->CreatedDate }}</td>
        <td>{{ $item->UpdateDate }}</td>
        <td>
            <a onclick = "updatePage(this);" data-url="{{ route('admin.post_cat.edit', $item->id) }}" href="javascript:void(0);" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> @lang('admin.edit')</a>
            <a href="#delete-{{$item->id}}" onclick="deletePostCat('{{ $item->id }}');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> @lang('admin.delete')</a>
        </td>
    </tr>
@endforeach()