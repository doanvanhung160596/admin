@extends('admin.layouts.index')
@section('title','Quản lý trang')
@section('content')
    <div class="boxed">

        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container">
            <div id="page-head">

                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 class="page-header text-overflow">@lang('admin.post')</h1>
                    <div class="text-right" style="margin-right: 30px;">
                        <button class="btn btn-primary" id="add_post">@lang('admin.add_post')</button>
                        <input type="hidden" id="add_post_hidden" value="{{route('admin.post.create')}}">
                    </div>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->


                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="{{ route('admin.home') }}"><i class="demo-pli-home"></i></a></li>
                    <li>Bài viết</li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->

            </div>


            <!--Page content-->
            <!--===================================================-->
            <div id="page-content">

                <!-- Basic Data Tables -->
                <!--===================================================-->
                <div class="panel data-table">
                    <div class="panel-heading">
                        <h3 class="panel-title data-table-title" style="text-transform: uppercase;">Danh sách bài viết</h3>
                    </div>
                    <div class="panel-body">
                        <select class="selectpicker" name="actions" id="actions">
                            <option value="1">Đăng</option>
                            <option value="-1">Chờ xét duyệt</option>
                            <option value="4">Xóa</option>
                        </select>
                        <button class="btn btn-primary" style="margin-bottom: 15px;" id="action">Thao Tác</button>
						<?php /**@var App\Datatables\PostDatatable $datatable */?>
                        {!! $dataTable->table() !!}
                    </div>
                </div>
                <!--===================================================-->
                <!-- End Striped Table -->
            </div>
            <!--===================================================-->
            <!--End page content-->

        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->
        <div class="modal" id="myModal">
            <div class="modal-dialog modal-lg" style="width: 80%;">
                <div class="modal-content">
                    <div class="loader loading-area">
                        <img class="loading-image" src="{{ asset('admin/img/loading/loading.gif') }}">
                        <p class="loading-text">@lang('admin.loading') ...</p>
                    </div>
                    <div class="modal-header" style="border-bottom: 1px solid #eee;">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">@lang('admin.add_post')</h4>
                    </div>
                    <div class="container"></div>
                    <div class="modal-body" id="modal-body-id">

                    </div>
                    {{--<div class="modal-footer">--}}
                    {{--<a href="#" data-dismiss="modal" class="btn btn-info">@lang('admin.close')</a>--}}
                    {{--<button class="btn btn-primary" id="save">@lang('admin.save')</button>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
        {{--@include('admin.layouts.aside')--}}
        {{--@include('admin.layouts.navigation')--}}
    </div>
@stop

@section('js')
    {!! $dataTable->scripts() !!}
    <script>
        function updateStatus(id){
            $info = iziToast.question({
                overlay: true,
                toastOnce: true,
                id: 'question',
                title: '@lang('admin.hi')',
                message: '@lang('admin.action_question')',
                position: 'center',
                buttons: [
                    ['<button><b>@lang('admin.yes')</b></button>', function (instance, toast) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            url: "{{route('admin.post.updateStatus')}}",
                            dataType: 'text',
                            data: {
                                id : id
                            },
                            beforeSend: function() {
                            },
                            complete: function() {
                            },
                            success: function(data) {
                                window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                                setTimeout(function(){
                                    iziToast.success({
                                        position: 'topCenter',
                                        title: '@lang('admin.info')',
                                        message: data
                                    });
                                }, 1000);
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast);
                    }, true],
                    ['<button>@lang('admin.no')</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast);

                    }]
                ]
            });
        }
        // ADD PAGE MODAL
        $('#add_post').click(function () {
            var action = $('#add_post_hidden').val();
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.add_post')');

            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
        // VIEW PAGE MODAL
        function viewPost(obj){
            // console.log(obj);
            var action = obj.getAttribute('data-url');
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.view_post')');
            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {
                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // UPDATE PAGE MODAL
        function updatePost(obj){
            var action = obj.getAttribute('data-url');
            $('#myModal').modal('show');
            $('#modal-body-id').html('');
            $('#myModal .modal-title').text('@lang('admin.edit_post')');
            $.ajax({
                url: action,
                dataType: 'html',
                method: 'GET',
                data: {

                },
                beforeSend: function () {
                },
                complete: function () {
                },
                success: function (data) {
                    $('#modal-body-id').prepend(data);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        // Delete Page
        function deletePost(id){
            $info = iziToast.question({
                overlay: true,
                toastOnce: true,
                id: 'question',
                title: '@lang('admin.hi')',
                message: '@lang('admin.action_question')',
                position: 'center',
                buttons: [
                    ['<button><b>@lang('admin.yes')</b></button>', function (instance, toast) {
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            method: 'POST',
                            url: "{{route('admin.post.deletePost')}}",
                            dataType: 'json',
                            data: {
                                id : id
                            },
                            beforeSend: function() {
                            },
                            complete: function() {
                            },
                            success: function(response) {
                                if(response.result == 'success'){
                                    window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                                    setTimeout(function(){
                                        iziToast.success({
                                            position: 'topCenter',
                                            title: '@lang('admin.info')',
                                            message: response.info
                                        });
                                    }, 1000);
                                }else{
                                    window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                                    setTimeout(function(){
                                        iziToast.error({
                                            position: 'topCenter',
                                            title: '@lang('admin.info')',
                                            message: response.info
                                        });
                                    }, 1000);
                                }
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                            }
                        });
                        instance.hide({ transitionOut: 'fadeOut' }, toast);
                    }, true],
                    ['<button>@lang('admin.no')</button>', function (instance, toast) {

                        instance.hide({ transitionOut: 'fadeOut' }, toast);

                    }]
                ]
            });
        }
        // LIST CHECKBOX ACTION
        $('#checkAll').click(function () {
            var checkBoxes = $(".checkItem");
            checkBoxes.prop("checked", !checkBoxes.prop("checked"));
        });
        // ACTION STATUS AND DELETE
        $('#action').click(function () {
            var action = $('#actions').val();
            var item_checked = [];
            $.each($("input[name='checkItem']:checked"), function(){
                item_checked.push($(this).val());
            });
            // console.log(item_checked);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                method: 'POST',
                url: "{{route('admin.post.action')}}",
                dataType: 'json',
                data: {
                    action       : action,
                    item_checked : item_checked
                },
                beforeSend: function() {
                },
                complete: function() {
                },
                success: function(response) {
                    if(response.result == 'success'){
                        window.LaravelDataTables["dataTableBuilder"].ajax.reload();
                        setTimeout(function(){
                            iziToast.success({
                                position: 'topCenter',
                                title: '@lang('admin.info')',
                                message: response.info
                            });
                        }, 1000);
                    }else{
                        // setTimeout(function(){
                        iziToast.error({
                            position: 'topCenter',
                            title: '@lang('admin.info')',
                            message: response.info
                        });
                        // }, 1000);
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        });
    </script>
@stop
