<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PageDatatable;
use App\Http\Requests\CreateRequest;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use View;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;
use App\Model\Page;
use App\Model\Admin;
use PhpOffice\PhpSpreadsheet\Calculation\Database;

class PageController extends Controller
{

    protected $builder;

	/**
	 * PageController constructor.
	 * @param Builder $builder
	 */
	public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @param PageDatatable $datatable
	 * @return \Illuminate\Http\Response
	 */
    public function index(PageDatatable $datatable)
    {
		return $datatable->render('admin.page.page');
//		return view('admin.page.page',compact('html'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param Request $request
	 * @return void
	 */

	public function updateStatus(Request $request)
	{
		$page = Page::findOrFail($request->id);
		if($page->isactived())
		{
			$page->pending();
			return trans('admin.pending_success');
		}
			$page->active();
			return trans('admin.active_success');
	}

	public function deletePage(Request $request)
	{
		$page = Page::findOrFail($request->id);
		if($page->isactived())
		{
			$info =  trans('admin.delete_page_fail');
			return $data = response()->json(array(
				'result' => 'fail',
				'info'   => $info
			));
		}
		$page->delete();
		$info =  trans('admin.delete_page_success');
		return $data = response()->json(array(
			'result' => 'success',
			'info'   => $info
		));
	}


	/**
	 * @param CreateRequest $request
	 * @return \Illuminate\Http\Response
	 */
	public function create()
    {
		return $this->getAjaxForm($id = null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

	public function getAjaxForm($id = null) {
		if ($id) {
			$action = route('admin.page.update',$id);
			$page = Page::where('id',$id)->first();
			return response()->view('admin.page.page_popup',compact('action','id','page'));
		}
		$action = route('admin.page.store');
		return response()->view('admin.page.page_popup',compact('action','id'));
	}

    public function store(Request $request)
    {
		$validator = \Validator::make($request->all(), [
			'title' 	=> 'required|unique:pages,title',
			'content' 	=> 'required',
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		$data=$request->all();
		$data['status'] = (string) $data['status'];
		$data['slug'] = str_slug($data['title']);
		$data['user_id']= auth('admin')->user()->id;
		Page::create($data);
		return response()->json([
			'result' =>'add',
			'success'=>trans('admin.add_page_success')
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$page = $page = Page::with('user')->where('id',$id)->first();
		return response()->view('admin.page.page_detail',compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return $this->getAjaxForm($id);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
		$validator = \Validator::make($request->all(), [
			'title' => [
				'required',
				Rule::unique('pages')->ignore($page->id),
			],
			'content' 	=> 'required',
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		$data=$request->all();
		$data['status'] = (string) $data['status'];
		$data['slug'] = str_slug($data['title']);
		$page->update($data);
		activity()
			->performedOn($page)
			->causedBy( auth('admin')->user()->id)
			->log('Trang <a href="'.route('admin.page.edit',$page->id).'">'.$page->title.'</a> đã chỉnh sửa');
		return response()->json([
			'result' =>'update',
			'success'=>trans('admin.update_page_success')
		]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function action(Request $request){
		$action = $request->action;
		$item_checked = $request->item_checked;
		if(empty($item_checked)){
			return response()->json([
				'result' => 'fail',
				'info'   => trans('admin.error_select_item')
			]);
		}
//		Delete Status
		if($action == 4){
			$count = 0;
			foreach ($item_checked as $item){
				$page = Page::findOrFail($item);
				if($page->status != '1'){
					$page->delete();
					$count ++;
				}
			}
			return response()->json([
				'result' => 'success',
				'info'   => 'Đã xóa '.$count.' mục trong danh sách'
			]);
		}
//		Update Status
			$count = 0;
			foreach ($item_checked as $item){
				$page = Page::findOrFail($item);
				if($page->status != "$action"){
					$page->update(['status' => "$action"]);;
					$count ++;
				}
			}
			return response()->json([
				'result' => 'success',
				'info'   =>'Cập nhật '.$count.' mục trong danh sách'
				]);
	}
}
