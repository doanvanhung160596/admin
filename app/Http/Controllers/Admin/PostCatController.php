<?php

namespace App\Http\Controllers\Admin;

use App\Model\Post_cat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class PostCatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admin.post.post_cat');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getListPostCat(){
		$post_multi_cat= new Post_cat();
		$post_cat=Post_cat::with('user')->get();
		$data=$post_multi_cat->multi_data_post($post_cat);
		return response()->view('admin.post.list_post_cat',compact('data'));
	}

	public function getListParent(){
		$parents = Post_cat::where('parent_id',0)->get();
		$id = $_GET['id'];
    	if($id){
			$post_cat = Post_cat::where('id',$id)->first();
			return response()->view('admin.post.list_parent',compact('parents','post_cat'));
		}
		return response()->view('admin.post.list_parent',compact('parents'));
	}

    public function create()
    {
		return $this->getAjaxForm($id = null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getAjaxForm($id = null){
		if ($id) {
			$action = route('admin.post_cat.update',$id);
			$post_cat = Post_cat::where('id',$id)->first();
			return response()->view('admin.post.post_cat_popup',compact('action','id','post_cat'));
		}
		$action = route('admin.post_cat.store');
		return response()->view('admin.post.post_cat_popup',compact('action','id'));
	}

    public function store(Request $request)
    {
		$validator = \Validator::make($request->all(), [
			'title' 	=> 'required|unique:post_cats,title',
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		$data=$request->all();
		$data['slug'] = str_slug($data['title']);
		$data['user_id']= auth('admin')->user()->id;
		Post_cat::create($data);
		return response()->json([
			'result' =>'add',
			'success'=>trans('admin.add_post_cat_success')
		]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		return $this->getAjaxForm($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post_cat $post_cat)
    {
		$validator = \Validator::make($request->all(), [
			'title' => [
				'required',
				Rule::unique('post_cats')->ignore($post_cat->id),
			],
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		$data=$request->all();
		$data['slug'] = str_slug($data['title']);
		$post_cat->update($data);
		activity()
			->performedOn($post_cat)
			->causedBy( auth('admin')->user()->id)
			->log('Danh mục <a href="'.route('admin.page.edit',$post_cat->id).'">'.$post_cat->title.'</a> đã chỉnh sửa');
		return response()->json([
			'result' =>'update',
			'success'=>trans('admin.update_post_cat_success')
		]);
    }

    public function deletePostCat(Request $request){
		$count_item = Post_cat::where('parent_id',$request->id)->count();
		$post_cat = Post_cat::findOrFail($request->id);
		if($count_item < 1){
			$post_cat->delete();
			return $data = response()->json(array(
				'result' => 'success',
				'info'   => trans('admin.delete_post_cat')
			));
		}
		return response()->json([
			'result' => 'fail',
			'info'   => 'Danh mục '.$post_cat->title.' tồn tại danh mục con'
		]);
	}

	public function action(Request $request){
		$action = $request->action;
		$item_checked = $request->item_checked;
		if(empty($item_checked)){
			return response()->json([
				'result' => 'fail',
				'info'   => trans('admin.error_select_item')
			]);
		}
//		Delete Post Cat
		if($action == 4){
			$count = 0;
			foreach ($item_checked as $item){
				$count_item = Post_cat::where('parent_id',$item)->count();
				$post_cat = Post_cat::findOrFail($item);
				if($count_item < 1){
					$post_cat->delete();
					$count ++;
				}else{
					return response()->json([
						'result' => 'fail',
						'info'   => 'Danh mục '.$post_cat->title.' tồn tại danh mục con'
					]);
				}
			}
			return response()->json([
				'result' => 'success',
				'info'   => 'Đã xóa '.$count.' mục trong danh sách'
			]);
		}
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
