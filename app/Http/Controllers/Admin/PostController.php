<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\PostDatatable;
use App\Model\Post;
use App\Model\Post_cat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class PostController extends Controller
{

	protected $builder;

	/**
	 * PageController constructor.
	 * @param Builder $builder
	 */
	public function __construct(Builder $builder)
	{
		$this->builder = $builder;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param PostDatatable $datatable
	 * @return \Illuminate\Http\Response
	 */
	public function index(PostDatatable $datatable)
	{
		return $datatable->render('admin.post.post');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return $this->getAjaxForm($id = null);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function getAjaxForm($id= null){
		$categories = Post_cat::where('parent_id','>',0)->get();
		if ($id) {
			$action = route('admin.post.update',$id);
			$post = Post::where('id',$id)->first();
			return response()->view('admin.post.post_popup',compact('action','id','post','categories'));
		}
		$action = route('admin.post.store');
		return response()->view('admin.post.post_popup',compact('action','id','categories'));
	}

	public function store(Request $request)
	{
		$validator = \Validator::make($request->all(), [
			'title' 		=> 'required|unique:posts,title',
			'description' 	=> 'required',
			'content' 		=> 'required',
			'post_cat_id' 	=> 'required',
			'status' 		=> 'required',
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}
		$data = $request->all();
		if ($request->hasFile('fileUpload')) {
			$file = $request->fileUpload;
			$fileName=$file->getClientOriginalName();
			$file->move(public_path('uploads/post'),$fileName);
			$data['image']='uploads/post/'.$fileName;
			$data['slug']=str_slug($data['title']);
			$data['title_seal']=str_slug($data['title'],' ');
			$data['user_id']= auth('admin')->user()->id;
			$data['category_id']=Post_cat::where('id', $request->post_cat_id)->first()->parent_id;
		}
		Post::create($data);
		return response()->json([
			'result' =>'add',
			'success'=>trans('admin.add_post_success')
		]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$post = Post::with('user')->where('id',$id)->first();
		return response()->view('admin.post.post_detail',compact('post'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		return $this->getAjaxForm($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param Post $post
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Post $post)
	{
		$validator = \Validator::make($request->all(), [
			'title' => [
				'required',
				Rule::unique('posts')->ignore($post->id),
			],
			'description' 	=> 'required',
			'content' 		=> 'required',
			'post_cat_id' 	=> 'required',
			'status' 		=> 'required',
		]);
		if ($validator->fails())
		{
			return response()->json(['errors'=>$validator->errors()->all()]);
		}

		$data=$request->all();
		if ($request->hasFile('fileUpload')) {
			$file = $request->fileUpload;
			$fileName=$file->getClientOriginalName();
			$file->move(public_path('uploads/post'),$fileName);
			$data['slug']=str_slug($data['title']);
			$data['title_seal']=str_slug($data['title'],' ');
			$data['image']='uploads/post/'.$fileName;
			$data['user_id']= auth('admin')->user()->id;
			$data['category_id']=Post_cat::where('id', $request->post_cat_id)->first()->parent_id;
		}else{
			$data['user_id']= auth('admin')->user()->id;
			$data['category_id']=Post_cat::where('id', $request->post_cat_id)->first()->parent_id;
		}
		$post->update($data);
		activity()
			->performedOn($post)
			->causedBy(auth('admin')->user())
			->log('Bài viết <a href="'.route('admin.post.show',$post->id).'">'.$post->title.'</a> đã chỉnh sửa');
		return response()->json([
			'result' =>'update',
			'success'=>trans('admin.update_post_success')
		]);
	}

	public function updateStatus(Request $request)
	{
		$post = Post::findOrFail($request->id);
		if($post->isactived())
		{
			$post->pending();
			return trans('admin.pending_success');
		}
		$post->active();
		return trans('admin.active_success');
	}

	public function deletePost(Request $request)
	{
		$post = Post::findOrFail($request->id);
		if($post->isactived())
		{
			$info =  trans('admin.delete_post_fail');
			return $data = response()->json(array(
				'result' => 'fail',
				'info'   => $info
			));
		}
		$post->delete();
		$info =  trans('admin.delete_post_success');
		return $data = response()->json(array(
			'result' => 'success',
			'info'   => $info
		));
	}

	public function action(Request $request){
		$action = $request->action;
		$item_checked = $request->item_checked;
		if(empty($item_checked)){
			return response()->json([
				'result' => 'fail',
				'info'   => trans('admin.error_select_item')
			]);
		}
//		Delete Status
		if($action == 4){
			$count = 0;
			foreach ($item_checked as $item){
				$post = Post::findOrFail($item);
				if($post->status != '1'){
					$post->delete();
					$count ++;
				}
			}
			return response()->json([
				'result' => 'success',
				'info'   => 'Đã xóa '.$count.' mục trong danh sách'
			]);
		}
//		Update Status
		$count = 0;
		foreach ($item_checked as $item){
			$post = Post::findOrFail($item);
			if($post->status != "$action"){
				$post->update(['status' => "$action"]);;
				$count ++;
			}
		}
		return response()->json([
			'result' => 'success',
			'info'   =>'Đã cập nhật trạng thái '.$count.' mục trong danh sách'
		]);
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
}
