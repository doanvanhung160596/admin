<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * @property  datatables
 */
class PageController extends Controller
{
    public function index(){
		$pages = Page::latest()->with('user')->paginate(25);
		return view('admin.page.page', compact('pages'));
	}
}
