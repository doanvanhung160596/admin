<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;
class Post_cat extends Model
{
	protected $table = "post_cats";
	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title', 'slug', 'parent_id','user_id'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function user()
	{
		return $this->belongsTo('App\Model\Admin', 'user_id', 'id');
	}

	/**
	 * @return mixed
	 */
	public function getCreatedDateAttribute(){
		return $this->created_at->format('d/m/Y');
	}

	public function getUpdateDateAttribute(){
		return $this->updated_at->format('d/m/Y');
	}

	public function post(){
		return $this->hasMany('App\Model\Post','post_cat_id','id');
	}

	public function child()
	{
		return $this->hasMany('App\Model\Post_cat', 'parent_id');
	}

	public function parent()
	{
		return $this->belongsTo('App\Model\Post_cat','parent_id');
	}

	public  function multi_data_post($data,$parent_id=0,$level=0){
		$result = array();
		if(!empty($data)){
			foreach ($data as $item){
				if($item->parent_id==$parent_id){
					$item['level']=$level;
					$result[]=$item;
					$child=$this->multi_data_post($data,$item->id,$level+1);
					$result=array_merge($result,$child);
				}
			}
		}
		return $result;
	}

	public function checkParentId($id){

	}
}
