<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
class Page extends Model
{
	use LogsActivity;
	protected static $logAttributes = ['slug', 'title', 'status'];
	protected $table = "pages";
	public $aopends = ['created_date','updated_date','view'];
	protected $fillable = ['title','content', 'slug', 'status', 'content', 'user_id','viewer'];

	public function user()
	{
		return $this->belongsTo('App\Model\Admin', 'user_id', 'id');
	}
	public function active()
	{
		$this->update(['status' => '1']);
	}

	public function pending()
	{
		$this->update(['status' => '-1']);
	}

	public function isactived()
	{
		return $this->status == '1';
	}

	public function getCreatedDateAttribute(){
		return $this->created_at->diffForHumans();
	}

}
