<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Post extends Model
{
	use LogsActivity;
	protected static $logAttributes = ['slug', 'title', 'status'];
	protected $table = "posts";
	public $aopends = ['created_date','updated_date','view'];
	protected $fillable = ['title','title_seal','description','image','content', 'slug', 'status', 'content','post_cat_id','status','slug','category_id','user_id','viewer'];

	public function user()
	{
		return $this->belongsTo('App\Model\Admin', 'user_id', 'id');
	}

	public function post_cat(){
		return $this->belongsTo('App\Model\Post_cat','post_cat_id','id');
	}

	public function category(){
		return $this->belongsTo('App\Model\Post_cat','category_id','id');
	}

	public function active()
	{
		$this->update(['status' => '1']);
	}

	public function pending()
	{
		$this->update(['status' => '-1']);
	}

	public function isactived()
	{
		return $this->status == '1';
	}

	public function getCreatedDateAttribute(){
		return $this->created_at->diffForHumans();
	}
}
