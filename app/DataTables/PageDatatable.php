<?php

namespace App\DataTables;

use App\Model\Page;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class PageDatatable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables($query)
			->addColumn('created_at', function ($page) {
				return $page->created_at ? with(new Carbon($page->created_at))->format('d/m/Y') : '';
			})
			->addColumn('updated_at', function ($page) {
				return $page->updated_at ? with(new Carbon($page->updated_at))->format('d/m/Y') : '';
			})
			->filterColumn('created_at', function ($query, $keyword) {
				$query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
			})
			->filterColumn('updated_at', function ($query, $keyword) {
				$query->whereRaw("DATE_FORMAT(updated_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
			})
			->addColumn('viewer', function ($page) {
				return $page->viewer .trans('admin.view');
			})
			->addColumn('status', function ($page) {
				if($page->status == '1')
				{
					return '<span onclick="updateStatus(' .$page->id .');" style="cursor: pointer" class="label label-info">'.trans('admin.active').'</span>';
				}
				return '<span onclick="updateStatus(' .$page->id .');" style="cursor: pointer" class="label label-success">'.trans('admin.pending').'</span>';
			})
			->addColumn('action', function ($page) {
				return '
							<a onclick = "viewPage(this);" data-url="'. route('admin.page.show', $page->id) .'" href="javascript:void(0);" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-new-window"></i> '.trans('admin.view_port').'</a>
							<a onclick = "updatePage(this);" data-url="'. route('admin.page.edit', $page->id) .'" href="javascript:void(0);" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> '.trans('admin.edit').'</a>
							<a href="#delete-'.$page->id.'" onclick="deletePage(' .$page->id .');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> '.trans('admin.delete').'</a>
					';
			})
			->addColumn('checkbox', function ($page) {
				return '
					     	<input name="checkItem" value="'.$page->id.'" style="cursor: pointer;" class="checkItem magic-checkbox" type="checkbox" id="'.$page->id.'" />
							<label for="'.$page->id.'"></label>         
					';
			})
			->rawColumns(['action','status','viewer','checkbox']);
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param Page $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Page $model)
	{
		return $model->newQuery()->with(['user'])->select('pages.*');
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->minifiedAjax()
			->parameters($this->getBuilderParameters());
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return
			[
				[
					'defaultContent' => '<input type="checkbox" style="cursor: pointer;" class="magic-checkbox" name="checkAll" id="checkAll"><label for="checkAll"></label>',
					'title'          => '<input type="checkbox" style="cursor: pointer;" class="magic-checkbox" name="checkAll" id="checkAll"><label for="checkAll"></label>',
					'data'           => 'checkbox',
					'name'           => 'checkbox',
					'orderable'      => false,
					'searchable'     => false,
					'exportable'     => false,
					'printable'      => true,
					'width'          => '10px',
				],
//			['data' => 'checkbox', 'name'=> 'checkbox', 'orderable'=> false, 'searchable'=> false],
				['data' => 'id', 'name' => 'id', 'title' => 'ID'],
				['data' => 'title', 'name' => 'title', 'title' =>  trans('admin.title')],
				['data' => 'user.name', 'name' => 'user.name', 'title' => trans('admin.created_by')],
				['data' => 'status', 'name' => 'status', 'title' => trans('admin.status')],
				['data' => 'viewer', 'name' => 'viewer', 'title' => trans('admin.view')],
				['data' => 'created_at', 'name' => 'created_at', 'title' => trans('admin.created_at')],
				['data' => 'updated_at', 'name' => 'updated_at', 'title' => trans('admin.updated_at')],
				['data' => 'action', 'name'=> 'action','title' => trans('admin.action'), 'orderable'=> false, 'searchable'=> false]
			];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Page_' . date('YmdHis');
	}

	/**
	 * Get default builder parameters.
	 *
	 * @return array
	 */
	protected function getBuilderParameters()
	{
		return
			[
				'drawCallback' => 'function() { 
				$( "#checkAll" ).prop( "checked", false );
			 }',
				'language' => [
					'processing' => '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>',
					'url' => asset('admin/lang-datatable/'.app()->getLocale().'.json')
				]
			];
	}
}
