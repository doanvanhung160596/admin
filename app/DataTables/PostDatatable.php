<?php

namespace App\DataTables;

use App\Model\Post;
use Carbon\Carbon;
use Yajra\DataTables\Services\DataTable;

class PostDatatable extends DataTable {
	/**
	 * Build DataTable class.
	 *
	 * @param mixed $query Results from query() method.
	 * @return \Yajra\DataTables\DataTableAbstract
	 */
	public function dataTable($query)
	{
		return datatables($query)
			->addColumn('created_at', function ($post) {
				return $post->created_at ? with(new Carbon($post->created_at))->format('d/m/Y') : '';
			})
			->addColumn('updated_at', function ($post) {
				return $post->updated_at ? with(new Carbon($post->updated_at))->format('d/m/Y') : '';
			})
			->filterColumn('created_at', function ($query, $keyword) {
				$query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
			})
			->filterColumn('updated_at', function ($query, $keyword) {
				$query->whereRaw("DATE_FORMAT(updated_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
			})
			->addColumn('image', function ($post) {
				return '
					<img src="' . asset($post->image) . '" style="width:100px; height:80px;">
					';
			})
			->addColumn('viewer', function ($post) {
				return $post->viewer . trans('admin.view');
			})
			->addColumn('status', function ($post) {
				if ($post->status == '1')
				{
					return '<span onclick="updateStatus(' . $post->id .
						');" style="cursor: pointer" class="label label-info">' . trans('admin.active') . '</span>';
				}
				return '<span onclick="updateStatus(' . $post->id .
					');" style="cursor: pointer" class="label label-success">' . trans('admin.pending') . '</span>';
			})
			->addColumn('action', function ($post) {
				return '
							<a onclick = "viewPost(this);" data-url="' . route('admin.post.show', $post->id) .
					'" href="javascript:void(0);" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-new-window"></i> ' .
					trans('admin.view_port') . '</a>
							<a onclick = "updatePost(this);" data-url="' . route('admin.post.edit', $post->id) .
					'" href="javascript:void(0);" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> ' .
					trans('admin.edit') . '</a>
							<a href="#delete-' . $post->id . '" onclick="deletePost(' . $post->id .
					');" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> ' .
					trans('admin.delete') . '</a>
					';
			})
			->addColumn('checkbox', function ($post) {
				return '
					     	<input name="checkItem" value="' . $post->id .
					'" style="cursor: pointer;" class="checkItem magic-checkbox" type="checkbox" id="' . $post->id . '" />
							<label for="' . $post->id . '"></label>         
					';
			})
			->rawColumns(['action', 'status', 'image', 'viewer', 'checkbox']);
	}

	/**
	 * Get query source of dataTable.
	 *
	 * @param Post $model
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function query(Post $model)
	{
		return $model->newQuery()->with(['user', 'category', 'post_cat'])->select('posts.*');
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\DataTables\Html\Builder
	 */
	public function html()
	{
		return $this->builder()
			->columns($this->getColumns())
			->minifiedAjax()
			->parameters($this->getBuilderParameters());
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	protected function getColumns()
	{
		return
			[
				[
					'defaultContent' => '<input type="checkbox" style="cursor: pointer;" class="magic-checkbox" name="checkAll" id="checkAll"><label for="checkAll"></label>',
					'title' => '<input type="checkbox" style="cursor: pointer;" class="magic-checkbox" name="checkAll" id="checkAll"><label for="checkAll"></label>',
					'data' => 'checkbox',
					'name' => 'checkbox',
					'orderable' => FALSE,
					'searchable' => FALSE,
					'exportable' => FALSE,
					'printable' => TRUE,
					'width' => '10px',
				],
				['data' => 'id', 'name' => 'id', 'title' => 'ID'],
				['data' => 'title', 'name' => 'title', 'title' => trans('admin.title')],
				['data' => 'user.name', 'name' => 'user.name', 'title' => trans('admin.created_by')],
				['data' => 'category.title', 'name' => 'category.title', 'title' => trans('admin.category_high')],
				['data' => 'post_cat.title', 'name' => 'post_cat.title', 'title' => trans('admin.category_post')],
				['data' => 'status', 'name' => 'status', 'title' => trans('admin.status')],
				['data' => 'image', 'name' => 'image', 'title' => trans('admin.image')],
				['data' => 'viewer', 'name' => 'viewer', 'title' => trans('admin.view')],
				['data' => 'created_at', 'name' => 'created_at', 'title' => trans('admin.created_at')],
				['data' => 'updated_at', 'name' => 'updated_at', 'title' => trans('admin.updated_at')],
				[
					'data' => 'action',
					'name' => 'action',
					'title' => trans('admin.action'),
					'orderable' => FALSE,
					'searchable' => FALSE
				]
			];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename()
	{
		return 'Post_' . date('YmdHis');
	}

	/**
	 * Get default builder parameters.
	 *
	 * @return array
	 */
	protected function getBuilderParameters()
	{
		return
			[
				'drawCallback' => 'function() { 
				$( "#checkAll" ).prop( "checked", false );
			 }',
				'language' => [
					'processing' => '<div class="overlay"><i class="fa fa-refresh fa-spin"></i></div>',
					'url' => asset('admin/lang-datatable/'.app()->getLocale().'.json')
				],
			];
	}
}
