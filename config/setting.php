<?php

return [

    'route' => [
        'enable_register' => true,
        'enable_login_social' => false,
    ],

    'admin_name' => [
        'en' => 'Admin',
        'vi' => 'Quản trị',
    ],
];
