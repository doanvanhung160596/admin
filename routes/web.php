<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Spatie\Activitylog\Models\Activity;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::namespace('admin')->prefix('admin')->name('admin.')->group(function (){
	Auth::routes();
	Route::get('/home', 'HomeController@index')->name('home');
//	ADMIN_PAGE
	Route::resource('/page','PageController');
	Route::post('/page/updateStatus', 'PageController@updateStatus')->name('page.updateStatus');
	Route::post('/page/deletePage', 'PageController@deletePage')->name('page.deletePage');
	Route::get('/page/getAjaxForm', 'PageController@getAjaxForm')->name('page.getAjaxForm');
	Route::post('/page/action', 'PageController@action')->name('page.action');
// ADMIN_POST CAT
	Route::resource('/post_cat','PostCatController');
	Route::get('/postCat/getListPostCat', 'PostCatController@getListPostCat')->name('postCat.getListPostCat');
	Route::get('/postCat/getListParent', 'PostCatController@getListParent')->name('postCat.getListParent');
	Route::post('/postCat/deletePostCat', 'PostCatController@deletePostCat')->name('postCat.deletePostCat');
	Route::post('/postCat/action', 'PostCatController@action')->name('postCat.action');
//	ADMIN POST
	Route::resource('/post','PostController');
	Route::post('/post/updateStatus', 'PostController@updateStatus')->name('post.updateStatus');
	Route::post('/post/deletePost', 'PostController@deletePost')->name('post.deletePost');
	Route::post('/post/action', 'PostController@action')->name('post.action');

});
Route::get('/test',function (){
//	$activity = Activity::all()->last();
//	dd($activity->changes());
//	$data = auth('admin')->user()->activity;
//	dd($data);
});
//Route::namespace('ajax')->prefix('ajax')->name('ajax.')->group(function (){
//	Route::post('/page', 'PageController@index')->name('page');
//});
